import * as React from 'react'
import BootSplash from 'react-native-bootsplash'

import { NavigationContainer } from '@react-navigation/native'
import { MainStackComponent } from '../navigation/navigation'

export const App = () => {
  React.useEffect(() => {
    BootSplash.hide()
  }, [])

  return (
    <NavigationContainer>
      <MainStackComponent />
    </NavigationContainer>
  )
}
