import { ICountry } from '../interface/ICountry'
export enum Country {
  Poland,
  Germany,
  France,
  Spain,
  'United Kingdom',
}
export const Countries: ICountry[] = [
  { name: Country.Poland, id: '1' },
  { name: Country.Germany, id: '2' },
  { name: Country.France, id: '3' },
  { name: Country.Spain, id: '4' },
  { name: Country['United Kingdom'], id: '5' },
]
