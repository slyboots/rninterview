import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'

class AxiosInstance {
  private axios = axios.create({ baseURL: 'http://universities.hipolabs.com' })

  public get = <T = any>(...params: [string, AxiosRequestConfig?]): Promise<AxiosResponse<T>> =>
    this.axios.get<T>(...params)
}

export default AxiosInstance
