import AxiosInstance from './AxiosInstance'
import { Country } from '../consts/countries'
import { IUniversity } from '../interface/IUniversity'
import { SEARCH } from './endpoint'

class HTTPService {
  private axios = new AxiosInstance()
  public getUniversityByCountry = (country: Country) =>
    this.axios.get<IUniversity[]>(SEARCH, { params: { country } })
  public getUniversityByCountryAndByName = (country: Country, name: string) =>
    this.axios.get<IUniversity[]>(SEARCH, { params: { country, name } })
}

export default new HTTPService()
