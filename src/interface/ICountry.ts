import { Country } from '../consts/countries'

export interface ICountry {
  id: string
  name: Country
}
