export interface IUniversity {
  alpha_two_code: string
  domains: string[]
  country: string
  'state-province': any
  web_pages: string[]
  name: string
}
