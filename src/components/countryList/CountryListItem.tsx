import { ListRenderItem, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { ICountry } from '../../interface/ICountry'
import React from 'react'
import { Colors } from '../../consts/colors'
type Props = {
  country: ICountry
  onItemClickHandler: (country: ICountry) => void
}
export const CountryListItem: ListRenderItem<Props> = ({ item }) => (
  <View style={style.container}>
    <TouchableOpacity onPress={() => item.onItemClickHandler(item.country)}>
      <Text style={style.text}>{item.country.name}</Text>
    </TouchableOpacity>
  </View>
)

const style = StyleSheet.create({
  container: {
    marginTop: 16,
    paddingVertical: 16,
    borderWidth: 3,
    borderColor: Colors.DARK_BORDER,
    borderRadius: 30,
    backgroundColor: Colors.ORANGE_PRIME,
  },
  text: {
    color: Colors.DARK_BORDER,
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
})
