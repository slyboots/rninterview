import React from 'react'
import { FlatList, StyleSheet } from 'react-native'
import { CountryListItem } from './CountryListItem'
import { ICountry } from '../../interface/ICountry'

type Props = {
  countries: ICountry[]
  onItemClickHandler: (country: ICountry) => void
}
export const CountryList: React.FC<Props> = ({ countries, onItemClickHandler }) => {
  const items = countries.map(country => {
    return { country: country, onItemClickHandler: onItemClickHandler }
  })
  return (
    <FlatList
      data={items}
      renderItem={CountryListItem}
      keyExtractor={item => item.country.id}
      style={style.container}
    />
  )
}

const style = StyleSheet.create({
  container: {
    padding: 5,
  },
})
