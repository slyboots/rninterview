import { Linking, ListRenderItem, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { IUniversity } from '../../../interface/IUniversity'
import React from 'react'
import { Colors } from '../../../consts/colors'

export const UniversityListItem: ListRenderItem<IUniversity> = ({ item }) => (
  <TouchableOpacity onPress={() => Linking.openURL(item.web_pages[0])} style={style.container}>
    <Text style={style.text}>{item.name}</Text>
  </TouchableOpacity>
)

const style = StyleSheet.create({
  container: {
    marginTop: 2,
    paddingVertical: 2,
    borderWidth: 1,
    borderColor: Colors.DARK_BORDER,
    borderRadius: 5,
  },
  text: {
    color: Colors.DARK_BORDER,
    textAlign: 'center',
    fontSize: 10,
  },
})
