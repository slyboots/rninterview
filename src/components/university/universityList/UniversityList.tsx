import React from 'react'
import { FlatList, Text, ActivityIndicator } from 'react-native'
import { IUniversity } from '../../../interface/IUniversity'
import { UniversityListItem } from './UniversityListItem'

type Props = {
  universities: IUniversity[]
  isLoading: boolean
}
export const UniversityList: React.FC<Props> = ({ universities, isLoading }) => {
  return (
    <FlatList
      data={universities}
      renderItem={UniversityListItem}
      keyExtractor={item => item.name}
      refreshing={isLoading}
      ListEmptyComponent={isLoading ? <ActivityIndicator /> : <Text>empty</Text>}
    />
  )
}
