import React, { useState } from 'react'
import { Button, StyleSheet, Text, TextInput, View } from 'react-native'
type Props = {
  onEndEditingHandler: (name: string) => void
  sortByNameHandler: () => void
}
export const UniversityHeader: React.FC<Props> = ({ onEndEditingHandler, sortByNameHandler }) => {
  const [query, setQuery] = useState('medical')
  return (
    <View>
      <TextInput
        onChangeText={setQuery}
        value={query}
        style={styles.input}
        onEndEditing={() => onEndEditingHandler(query)}
      />
      <Text>Posortuj</Text>
      <Button title={'Sortuj'} onPress={sortByNameHandler} />
    </View>
  )
}
const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
})
