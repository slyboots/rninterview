import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import * as screenComponents from './screens/index'
import { ICountry } from '../interface/ICountry'
import { Colors } from '../consts/colors'

export type MainStackParamList = {
  COUNTRY_LIST_SCREEN: undefined
  COUNTRY_DETAILS_SCREEN: {
    country: ICountry
  }
}

const MainStack = createNativeStackNavigator<MainStackParamList>()

export const MainStackComponent = () => {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name={'COUNTRY_LIST_SCREEN'}
        component={screenComponents.CountryListScreen}
        options={{ headerShown: false }}
      />
      <MainStack.Screen
        name={'COUNTRY_DETAILS_SCREEN'}
        component={screenComponents.CountryDetailsScreen}
        options={({ route }) => ({
          title: route.params.country.name,
          headerStyle: {
            backgroundColor: Colors.ORANGE_PRIME,
          },
          headerTitleAlign: 'center',
          headerTintColor: 'white',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </MainStack.Navigator>
  )
}
