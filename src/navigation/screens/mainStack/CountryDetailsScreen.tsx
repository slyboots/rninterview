import React, { useCallback, useEffect, useState } from 'react'
import { View, Text } from 'react-native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { MainStackParamList } from '../../navigation'
import { UniversityList } from '../../../components/university/universityList/UniversityList'
import HTTPService from '../../../service/HTTPService'
import { IUniversity } from '../../../interface/IUniversity'
import { UniversityHeader } from '../../../components/university/universityHeader/UniversityHeader'

type NavigationPropsType = NativeStackScreenProps<MainStackParamList, 'COUNTRY_DETAILS_SCREEN'>

const CountryListScreen = ({ route }: NavigationPropsType) => {
  const { country } = route.params
  const [error, setError] = useState('')
  const [universities, setUniversities] = useState<IUniversity[]>([])
  const [isLoading, setIsLoading] = useState(false)
  //TODO pagination
  const fetchUniversities = useCallback(() => {
    setIsLoading(true)
    HTTPService.getUniversityByCountry(country.name)
      .then(({ data }) => {
        setUniversities(data)
        setError('')
      })
      .catch((newError: any) => setError(newError))
      .finally(() => setIsLoading(false))
  }, [])
  const fetchUniversitiesByNames = (query: string) => {
    setIsLoading(true)
    HTTPService.getUniversityByCountryAndByName(country.name, query)
      .then(({ data }) => {
        setUniversities(data)
        setError('')
      })
      .catch((newError: any) => setError(newError))
      .finally(() => setIsLoading(false))
  }

  useEffect(fetchUniversities, [fetchUniversities])
  const sortByNameHandler = () => {
    setUniversities(universities.sort((uni1, uni2) => uni1.name.localeCompare(uni2.name)))
  }

  const onEndEditingHandler = (name: string) => {
    fetchUniversitiesByNames(name)
  }
  const errorContent = error ? <Text>{JSON.stringify(error)}</Text> : null
  return (
    <View>
      <UniversityHeader
        onEndEditingHandler={onEndEditingHandler}
        sortByNameHandler={sortByNameHandler}
      />
      <UniversityList universities={universities} isLoading={isLoading} />
      {errorContent}
    </View>
  )
}
export default CountryListScreen
