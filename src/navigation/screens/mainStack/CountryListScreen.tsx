import React from 'react'
import { View } from 'react-native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { Countries } from '../../../consts/countries'
import { MainStackParamList } from '../../navigation'
import { CountryList } from '../../../components/countryList/CountryList'
import { ICountry } from '../../../interface/ICountry'

type NavigationPropsType = NativeStackScreenProps<MainStackParamList, 'COUNTRY_LIST_SCREEN'>

const CountryListScreen = ({ navigation }: NavigationPropsType) => {
  const navigateToCountryDetails = (country: ICountry) => {
    //for type script 3.8.4
    //navigation.navigate({ key: 'COUNTRY_DETAILS_SCREEN', name: 'COUNTRY_DETAILS_SCREEN' })
    navigation.navigate('COUNTRY_DETAILS_SCREEN', { country: country })
  }
  return (
    <View>
      <CountryList countries={Countries} onItemClickHandler={navigateToCountryDetails} />
    </View>
  )
}
export default CountryListScreen
